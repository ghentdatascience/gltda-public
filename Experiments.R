### Set-up ### 

# Set wd
setwd("~/git_tda/Code/R_LTDA")
# Load libraries
library("ggplot2")
library("gridExtra")
library("gsubfn")
library("TDA")
# Source functions
source("VRgraph.R")
source("classLG.R")
source("constructGT.R")
source("splitBranch.R")
source("MGR.R")

### Local-global analysis of ECML-PKDD acronym ###

# Load and plot data

df.A <- read.table("ECMLPKDD.csv", sep = ",")
ggplot(df.A, aes(x = V1, y = V2)) + geom_point() + coord_fixed()

# Construct and visualize Vietoris-Rips graph from data

D.A <- dist(df.A)
eps <- 3.5
vr.A <- VRgraph(D.A, eps)
E <- sapply(E(vr.A), function(e) as.vector(get.edges(vr.A, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.A[e[1],], df.A[e[2],]))), ncol = 4, byrow = TRUE))
ggplot(df.A, aes(x = V1, y = V2)) + coord_fixed() +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), 
               color = 'red') + geom_point(size = 3, alpha = 0.25)

# Classify and visualize local-global topologies

start_time <- Sys.time()
lg.A <- classLG(vr.A, 3)
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
lg.A.text <- paste("(", lg.A$degree, ", ", lg.A$cycles, ")", sep = "")
ggplot(cbind(df.A, lg.A.text), aes(x = V1, y = V2, colour = lg.A.text)) +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), color = 'black') +
  geom_point(size = 3, alpha = 0.25) + 
  geom_point(size = 3) + theme_bw(base_size = 15) + 
  coord_fixed() + guides(colour = guide_legend("(Degree, Cycles)")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

# Construct underlying graph topology

start_time <- Sys.time()
GT.A <- constructGT(df.A, vr.A, lg.A, D.A, 4, method = "complete")
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
E <- sapply(E(GT.A), function(e) as.vector(get.edges(GT.A, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.A[GT.A$centers[e[1]],], df.A[GT.A$centers[e[2]],]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(df.A, lg.A.text, GT.A$membership), aes(x = V1, y = V2)) + 
  geom_point(size = 3, aes(shape = lg.A.text, colour = GT.A$membership)) +
  scale_shape_manual(values = seq(0, 7)) +
  geom_point(data = df.A[GT.A$centers,], col = 'black', size = 3) + 
  theme_bw(base_size = 15) + coord_fixed() + 
  guides(shape = guide_legend("(Degree, Cycles)"), 
         colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal") +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4))

# Extract and plot 8

df.8 <- df.A[which(df.A[,1] > 87.5 & df.A[,2] < 60),]
ggplot(df.8, aes(x = V1, y = V2)) + geom_point() + coord_fixed()

# Construct and visualize Vietoris-Rips graph from data

D.8 <- dist(df.8)
eps <- 3.5
vr.8 <- VRgraph(D.8, eps)
E <- sapply(E(vr.8), function(e) as.vector(get.edges(vr.8, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.8[e[1],], df.8[e[2],]))), ncol = 4, byrow = TRUE))
ggplot(df.8, aes(x = V1, y = V2)) + coord_fixed() + theme_bw(base_size = 15) +
  geom_segment(data = E, size = 1, aes(x = X1, y = X2, xend = X3, yend = X4), 
               color = 'red') + geom_point(size = 3, alpha = 0.5)

# Visualize the idea behind LTDA

circleFun <- function(center = c(0,0), r = 1, npoints = 100){
  tt <- seq(0, 2 * pi, length.out = npoints)
  xx <- center[1] + r * cos(tt)
  yy <- center[2] + r * sin(tt)
  return(data.frame(x = xx, y = yy))
}

c <- 64
I1 <- which(as.matrix(D.8)[c,] <= 5)
I2 <- which(as.matrix(D.8)[c,] > 10)
I3 <- setdiff(seq(nrow(df.8)), c(I1, I2))
ggplot() + coord_fixed() + theme_bw(base_size = 15) +
  geom_segment(data = E, size = 1, aes(x = X1, y = X2, xend = X3, yend = X4), color = 'black') + 
  geom_point(data = df.8[I1,], aes(x = V1, y = V2), size = 5, color = 'red', alpha = 0.75) +
  geom_point(data = df.8[I2,], aes(x = V1, y = V2), size = 5, color = 'blue', alpha = 0.75) +
  geom_point(data = df.8[I3,], aes(x = V1, y = V2), size = 5, color = 'green', alpha = 0.75) +
  geom_point(data = df.8[c,], aes(x = V1, y = V2), col = 'black', size = 7) + 
  geom_path(data = circleFun(as.numeric(df.8[c,]), 5, npoints = 100), aes(x = x, y = y),
            size = 3, alpha = 0.75) +
  geom_path(data = circleFun(as.numeric(df.8[c,]), 10, npoints = 100), aes(x = x, y = y),
            size = 3, alpha = 0.75)

### Local-global analysis of  point cloud toy data approaching a spiral ###

# Load and plot data

df.S <- read.table("spiral.csv", sep = ",")
ggplot(df.S, aes(x = V1, y = V2)) + geom_point() + coord_fixed()

# Construct and visualize Vietoris-Rips graph from data

D.S <- dist(df.S)
eps <- 3
vr.S <- VRgraph(D.S, eps)
E <- sapply(E(vr.S), function(e) as.vector(get.edges(vr.S, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.S[e[1],], df.S[e[2],]))), ncol = 4, byrow = TRUE))
ggplot(df.S, aes(x = V1, y = V2)) + coord_fixed() +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), 
               color = 'red') + geom_point(size = 3, alpha = 0.25)

# Classify and visualize local-global topologies

start_time <- Sys.time()
lg.S <- classLG(vr.S, 3)
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
lg.S.text <- paste("(", lg.S$degree, ", ", lg.S$cycles, ")", sep = "")
ggplot(cbind(df.S, lg.S.text), aes(x = V1, y = V2, colour = lg.S.text)) + 
  geom_point(size = 3) + theme_bw(base_size = 15) + 
  coord_fixed() + guides(colour = guide_legend("(Degree, Cycles)")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

# Construct underlying graph topology

start_time <- Sys.time()
GT.S <- constructGT(df.S, vr.S, lg.S, D.S, 3, method = "complete")
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
E <- sapply(E(GT.S), function(e) as.vector(get.edges(GT.S, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.S[GT.S$centers[e[1]],], df.S[GT.S$centers[e[2]],]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(df.S, lg.S.text, GT.S$membership), aes(x = V1, y = V2)) + 
  geom_point(size = 3, aes(shape = lg.S.text, colour = GT.S$membership)) +
  geom_point(data = df.S[GT.S$centers,], col = 'black', size = 3) + 
  theme_bw(base_size = 15) + coord_fixed() + 
  guides(shape = guide_legend("(Degree, Cycles)"), colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal") +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4))

# Divide long branch into consecutive components

GT.S <- splitBranch(GT.S, vr.S, D.S, 3, 30)
E <- sapply(E(GT.S), function(e) as.vector(get.edges(GT.S, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.S[GT.S$centers[e[1]], 1:2], df.S[GT.S$centers[e[2]], 1:2]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(df.S, lg.S.text, GT.S$membership), aes(x = V1, y = V2)) + 
  geom_point(size = 3, aes(shape = lg.S.text, colour = GT.S$membership)) +
  geom_point(data = df.S[GT.S$centers,], size = 4) + 
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), size = 2, alpha = 0.5) +
  theme_bw(base_size = 15) + coord_fixed() +
  guides(shape = guide_legend("(Degree, Cycles)"), colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

### Local-global analysis of noisy uneven distributed point cloud ###
### toy data approaching a simple graph-structured topological space ###

# Load and plot data

df.G <- read.table("df_Graph.csv", header = TRUE, sep = ",")
df.G[,1] <- NULL
ggplot(df.G, aes(x = x, y = y)) + geom_point() + coord_fixed()

# Construct and visualize Vietoris-Rips graph from data

D.G <- dist(df.G)
eps <- 0.3
vr.G <- VRgraph(D.G, eps)
E <- sapply(E(vr.G), function(e) as.vector(get.edges(vr.G, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.G[e[1],], df.G[e[2],]))), ncol = 4, byrow = TRUE))
ggplot(df.G, aes(x = x, y = y)) + coord_fixed() +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), 
               color = 'red') + geom_point(size = 3, alpha = 0.25)

# Classify and visualize local-global topologies

start_time <- Sys.time()
lg.G <- classLG(vr.G, 3)
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
lg.G.text <- paste("(", lg.G$degree, ", ", lg.G$cycles, ")", sep = "")
ggplot(cbind(df.G, lg.G.text), aes(x = x, y = y, colour = lg.G.text)) + 
  geom_point(size = 3) + theme_bw(base_size = 15) + 
  coord_fixed() + guides(colour = guide_legend("(Degree, Cycles)")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

# Construct underlying graph topology

start_time <- Sys.time()
GT.G <- constructGT(df.G, vr.G, lg.G, D.G, 6, method = "complete")
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
E <- sapply(E(GT.G), function(e) as.vector(get.edges(GT.G, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.G[GT.G$centers[e[1]],], df.G[GT.G$centers[e[2]],]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(df.G, lg.G.text, GT.G$membership), 
            aes(x = x, y = y)) + 
  geom_point(size = 3, aes(shape = lg.G.text, 
                           colour = GT.G$membership)) +
  geom_point(data = df.G[GT.G$centers,], col = 'black', size = 3) + 
  theme_bw(base_size = 15) + coord_fixed() + 
  guides(shape = guide_legend("(Degree, Cycles)"), 
         colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal") +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4))

### Local-global analysis of  point cloud toy data approaching an ellipse ###

# Load and plot data

df.E <- read.table("df_Ellipse.csv", header = TRUE, sep = ",")
df.E[,1] <- NULL
ggplot(df.E, aes(x = x, y = y)) + geom_point() + coord_fixed()

# Construct and visualize Vietoris-Rips graph from data

D.E <- dist(df.E)
eps <- 0.3
vr.E <- VRgraph(D.E, eps)
E <- sapply(E(vr.E), function(e) as.vector(get.edges(vr.E, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.E[e[1],], df.E[e[2],]))), ncol = 4, byrow = TRUE))
ggplot(df.E, aes(x = x, y = y)) + coord_fixed() +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), 
               color = 'red') + geom_point(size = 3, alpha = 0.25)

# Classify and visualize local-global topologies

start_time <- Sys.time()
lg.E <- classLG(vr.E, 2)
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
lg.E.text <- paste("(", lg.E$degree, ", ", lg.E$cycles, ")", sep = "")
ggplot(cbind(df.E, lg.E.text), aes(x = x, y = y, colour = lg.E.text)) + 
  geom_point(size = 3) + theme_bw(base_size = 15) +
  coord_fixed() + guides(colour = guide_legend("(Degree, Cycles)")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

# Construct underlying graph topology

start_time <- Sys.time()
GT.E <- constructGT(df.E, vr.E, lg.E, D.E, 6, method = "complete")
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
E <- sapply(E(GT.E), function(e) as.vector(get.edges(GT.E, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.E[GT.E$centers[e[1]],], df.E[GT.E$centers[e[2]],]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(df.E, lg.E.text, GT.E$membership), aes(x = x, y = y)) + 
  geom_point(size = 3, aes(shape = lg.E.text, colour = GT.E$membership)) +
  geom_point(data = df.E[GT.E$centers,], col = 'black', size = 3) + 
  theme_bw(base_size = 15) + coord_fixed() + 
  guides(shape = guide_legend("(Degree, Cycles)"), colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal") + 
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4))

### Local-global analysis of noisy uneven distributed point cloud ###
### toy data approaching a Y-structured topological space ###

# Load and plot data

df.Y <- read.table("toyY.csv", header = FALSE, sep = ",")
colnames(df.Y) <- c("x", "y")
ggplot(df.Y, aes(x = x, y = y)) + geom_point() + coord_fixed()

# Construct and visualize Vietoris-Rips graph from data

D.Y <- dist(df.Y)
eps <- 15
vr.Y <- VRgraph(D.Y, eps)
E <- sapply(E(vr.Y), function(e) as.vector(get.edges(vr.Y, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.Y[e[1],], df.Y[e[2],]))), ncol = 4, byrow = TRUE))
ggplot(df.Y, aes(x = x, y = y)) + coord_fixed() +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), 
               color = 'red') + geom_point(size = 3, alpha = 0.25)

# Classify and visualize local-global topologies

start_time <- Sys.time()
lg.Y <- classLG(vr.Y, 3)
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
lg.Y.text <- paste("(", lg.Y$degree, ", ", lg.Y$cycles, ")", sep = "")
ggplot(cbind(df.Y, lg.Y.text), aes(x = x, y = y, colour = lg.Y.text)) + 
         geom_point(size = 3) + theme_bw(base_size = 15) + 
  coord_fixed() + guides(colour = guide_legend("(Degree, Cycles)")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

# Construct underlying graph topology

start_time <- Sys.time()
GT.Y <- constructGT(df.Y, vr.Y, lg.Y, D.Y, 4, method = "complete")
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
E <- sapply(E(GT.Y), function(e) as.vector(get.edges(GT.Y, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.Y[GT.Y$centers[e[1]],], df.Y[GT.Y$centers[e[2]],]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(df.Y, lg.Y.text, GT.Y$membership), aes(x = x, y = y)) + 
  geom_point(size = 3, aes(shape = lg.Y.text, colour = GT.Y$membership)) +
  geom_point(data = df.Y[GT.Y$centers,], size = 5) + 
  theme_bw(base_size = 15) + coord_fixed() + 
  guides(shape = guide_legend("(Degree, Cycles)"), colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal") + 
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), size = 3, alpha = 1)

### Local-global analysis of noisy uneven distributed point cloud ###
### real data approaching a Y-structured topological space ###

# Load and plot data

df.stems <- read.table("stemcells.csv", header = TRUE, sep = ";")
df.stems.pca <- prcomp(df.stems[,-6], center = TRUE, scale. = TRUE)$x
df.stems.pca <- cbind(data.frame(df.stems.pca), df.stems$label)
colnames(df.stems.pca)[6] <- "label"
ggplot(df.stems.pca, aes(x = PC1, y = PC2, col = label)) + 
  geom_point(size = 5) + coord_fixed() + theme_bw(base_size = 15) +
  theme(legend.text = element_text(size = 25), legend.title = element_text(size = 25),
        legend.position = c(0.117, 0.15), legend.key.size = unit(1.5, 'lines')) +
  guides(colour = guide_legend(override.aes = list(size = 5), title = "Cell type")) +
  scale_color_manual(breaks = c("LT-HSC", "ST-HSC", "CMP", "CLP"), 
                     values = c("red", "yellow", "blue", "cyan"))

# Construct Vietoris-Rips graph from data
eps <- 2
D.stems <- dist(df.stems[,-6])
vr.stems <- VRgraph(D.stems, eps)

# Classify and visualize local-global topologies

start_time <- Sys.time()
lg.stems <- classLG(vr.stems, 2)
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
lg.stems.text <- paste("(", lg.stems$degree, ", ", lg.stems$cycles, ")", sep = "")
ggplot(cbind(df.stems.pca, lg.stems.text), aes(x = PC1, y = PC2, colour = lg.stems.text)) + 
  geom_point(size = 5) + theme_bw(base_size = 15) + 
  coord_fixed() + guides(colour = guide_legend("(Degree, Cycles)")) +
  theme(legend.text = element_text(size = 25), legend.title = element_text(size = 25),
        legend.position = c(0.2, 0.1), legend.key.size = unit(1.5, 'lines'))

# Construct underlying graph topology

start_time <- Sys.time()
GT.stems <- constructGT(df.stems, vr.stems, lg.stems, D.stems, 4, method = "mcquitty")
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
E <- sapply(E(GT.stems), function(e) as.vector(get.edges(GT.stems, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(df.stems.pca[GT.stems$centers[e[1]], 1:2], 
        df.stems.pca[GT.stems$centers[e[2]], 1:2]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(df.stems.pca, lg.stems.text, GT.stems$membership), aes(x = PC1, y = PC2)) + 
  geom_point(size = 5, aes(shape = lg.stems.text, colour = GT.stems$membership)) +
  geom_point(data = df.stems.pca[GT.stems$centers,], col = 'black', size = 4) + 
  theme_bw(base_size = 15) + coord_fixed() + 
  guides(shape = guide_legend("(Degree, Cycles)"), colour = guide_legend("Cluster")) +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), size = 2) +
  theme(legend.text = element_text(size = 25), legend.title = element_text(size = 25),
        legend.position = "right", legend.key.size = unit(1.5, 'lines'))

# Compare with other graph reconstruction method

start_time <- proc.time()
mgr.stems <- MGR(D.stems, 1)
proc.time() - start_time
ggplot(cbind(df.stems.pca, factor(mgr.stems$assignment)), 
       aes(x = PC1, y = PC2, colour = factor(mgr.stems$assignment))) + 
  geom_point(size = 5) + theme_bw(base_size = 15) + coord_fixed() + 
  guides(colour = guide_legend("branch (0) / edge (1)"))
plot(mgr.stems)

start_time <- proc.time()
mgr.stems <- MGR(as.dist(distances(vr.stems)), 1)
proc.time() - start_time
ggplot(cbind(df.stems.pca, factor(mgr.stems$assignment)), 
       aes(x = PC1, y = PC2, colour = factor(mgr.stems$assignment))) + 
  geom_point(size = 5) + theme_bw(base_size = 15) + coord_fixed() + 
  guides(colour = guide_legend("branch (0) / edge (1)"))
plot(mgr.stems)

### Local-global analysis of earthquake data ###

# Load and plot data

df.EQ <- rbind(read.table("query1.csv", header = TRUE, sep = ",", stringsAsFactors = FALSE),
               read.table("query2.csv", header = TRUE, sep = ",", stringsAsFactors = FALSE),
               read.table("query3.csv", header = TRUE, sep = ",", stringsAsFactors = FALSE),
               read.table("query4.csv", header = TRUE, sep = ",", stringsAsFactors = FALSE),
               read.table("query5.csv", header = TRUE, sep = ",", stringsAsFactors = FALSE),
               read.table("query6.csv", header = TRUE, sep = ",", stringsAsFactors = FALSE),
               read.table("query7.csv", header = TRUE, sep = ",", stringsAsFactors = FALSE))
ggplot(df.EQ, aes(x = longitude, y = latitude)) + geom_point() + coord_fixed()

# Preprocess data
df.EQ$longitude <- sapply(df.EQ$longitude, function(l){
  if(l < 0) return(l + 360) else return(l)
})
I <- (df.EQ$mag > 6.5 & df.EQ$longitude > 140 & df.EQ$longitude < 315 & 
        df.EQ$latitude < 65 & df.EQ$latitude > -75)
ggplot(df.EQ[I,], aes(x = longitude, y = latitude)) + geom_point() + coord_fixed()
DTM <- dtm(X = df.EQ[I, c("longitude", "latitude")], 
           Grid = df.EQ[I, c("longitude", "latitude")], m0 = 0.1)
J <- DTM < 30
ggplot(cbind(df.EQ[I,][J,], DTM[J]), aes(x = longitude, y = latitude, col = DTM[J])) +
  geom_point() + coord_fixed()
p_df.EQ <- df.EQ[I, c("longitude", "latitude")][J,]

# Construct and visualize Vietoris-Rips graph from data

D.EQ <- dist(p_df.EQ)
eps <- 10
vr.EQ <- VRgraph(D.EQ, eps)
E <- sapply(E(vr.EQ), function(e) as.vector(get.edges(vr.EQ, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(p_df.EQ[e[1],], p_df.EQ[e[2],]))), ncol = 4, byrow = TRUE))
ggplot(p_df.EQ, aes(x = longitude, y = latitude)) + coord_fixed() +
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), 
               color = 'red') + geom_point(size = 3, alpha = 0.25)

# Classify and visualize local-global topologies

start_time <- Sys.time()
lg.EQ <- classLG(vr.EQ, 2)
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
lg.EQ.text <- paste("(", lg.EQ$degree, ", ", lg.EQ$cycles, ")", sep = "")
ggplot(cbind(p_df.EQ, lg.EQ.text), aes(x = longitude, y = latitude, colour = lg.EQ.text)) + 
  geom_point(size = 3) + theme_bw(base_size = 15) + 
  coord_fixed() + guides(colour = guide_legend("(Degree, Cycles)")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

# Construct underlying graph topology

start_time <- Sys.time()
GT.EQ <- constructGT(p_df.EQ, vr.EQ, lg.EQ, D.EQ, 2, method = "complete")
end_time <- Sys.time()
message(paste("time: ", toString(end_time - start_time), "s", sep = ""))
E <- sapply(E(GT.EQ), function(e) as.vector(get.edges(GT.EQ, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(p_df.EQ[GT.EQ$centers[e[1]], 1:2], 
        p_df.EQ[GT.EQ$centers[e[2]], 1:2]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(p_df.EQ, lg.EQ.text, GT.EQ$membership), aes(x = longitude, y = latitude)) + 
  geom_point(size = 3, aes(shape = lg.EQ.text, colour = GT.EQ$membership)) +
  geom_point(data = p_df.EQ[GT.EQ$centers,], size = 4) + 
  theme_bw(base_size = 15) + coord_fixed() + 
  guides(shape = guide_legend("(Degree, Cycles)"), colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal") + 
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), size = 2, alpha = 0.5)

# Divide long branches into consecutive components

GT.EQ <- splitBranch(GT.EQ, vr.EQ, D.EQ, 8, 15)
GT.EQ <- splitBranch(GT.EQ, vr.EQ, D.EQ, 7, 5)
E <- sapply(E(GT.EQ), function(e) as.vector(get.edges(GT.EQ, e)))
E <- data.frame(matrix(unlist(apply(E, 2, function(e) 
  cbind(p_df.EQ[GT.EQ$centers[e[1]], 1:2], 
        p_df.EQ[GT.EQ$centers[e[2]], 1:2]))), 
  ncol = 4, byrow = TRUE))
ggplot(cbind(p_df.EQ, lg.EQ.text, GT.EQ$membership), aes(x = longitude, y = latitude)) + 
  geom_point(size = 3, aes(shape = lg.EQ.text, colour = GT.EQ$membership)) +
  geom_point(data = p_df.EQ[GT.EQ$centers,], size = 4, alpha = 0.5) + 
  geom_segment(data = E, aes(x = X1, y = X2, xend = X3, yend = X4), size = 2, alpha = 0.5) +
  theme_bw(base_size = 15) + coord_fixed() +
  guides(shape = guide_legend("(Degree, Cycles)"), colour = guide_legend("Cluster")) +
  theme(legend.position = "bottom", legend.direction = "horizontal")

# Compare with other graph reconstruction method

start_time <- proc.time()
mgr.EQ <- MGR(D.EQ, 5)
proc.time() - start_time
plot(mgr.EQ)

start_time <- proc.time()
mgr.EQ <- MGR(as.dist(distances(vr.EQ)), 5)
proc.time() - start_time
plot(mgr.EQ)