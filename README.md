# README #

## Local topological data analysis uncovers the global structure of data approaching one-dimensional stratified spaces ##

### What is this repository for? ###

* Experiment code for submission 189 to ECML PKDD 2018

### How do I get set up? ###

* Install RStudio

### How to run? ###

* Run script 'Experiments.R'

### Where to find experimental results? ### 

* Results can be seen after running the script